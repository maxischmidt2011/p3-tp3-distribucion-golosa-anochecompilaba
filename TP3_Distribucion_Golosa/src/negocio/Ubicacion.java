package negocio;

import java.io.Serializable;

public class Ubicacion implements Serializable {

	private static final long serialVersionUID = 1L;
	private double latitud;
	private double longitud;

	public Ubicacion(double latitud, double longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public Ubicacion() {
		this.latitud = 0;
		this.longitud = 0;
	}

	public double calcularDistancia(Ubicacion u) {
		 //return Math.sqrt(Math.pow(this.getLatitud() - u.getLatitud(), 2) + Math.pow(this.getLongitud() - u.getLongitud(), 2));	
		 double dist = distanciaCoord(this.getLatitud(), this.getLongitud(), u.getLatitud(), u.getLongitud());
		 return Math.round(dist*10000d)/10000d;
	}
	
	//https://donnierock.com/2015/03/16/calculando-la-distancia-entre-doos-coordenadas-en-java/
	private double distanciaCoord(double lat1, double lng1, double lat2, double lng2) {  
        double radioTierra = 6371;//en kilómetros  
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;  
    }  
	
	
	@Override
	public boolean equals(Object obj) {
		if (this.getClass() != obj.getClass())
			return false;
		Ubicacion otro = (Ubicacion) obj;
		if ((this.getLatitud() != otro.getLatitud()) && (this.getLongitud() != otro.getLongitud()))
			return false;
		else
			return true;
	}
	

	public double getLatitud() { return latitud; }

	public double getLongitud() { return longitud; }

}
