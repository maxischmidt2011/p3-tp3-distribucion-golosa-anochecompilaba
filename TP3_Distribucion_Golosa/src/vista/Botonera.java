package vista;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;

public class Botonera extends JPanel {

	private static final long serialVersionUID = 1L;

	public Botonera(Panel panel) {
		setLayout(null);

		JButton btnEdicion = new JButton("Tema");
		btnEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.cambiarTema();
			}
		});
		btnEdicion.setBounds(10, 11, 89, 23);
		add(btnEdicion);

		JButton btn_info = new JButton("Info");
		btn_info.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null,
						"     Segundo Semestre 2020\r\n" + "        Sanchez Juan Ignacio\r\n"
								+ "        Schmidt Maximiliano\r\n" + "          Sosa Mart�n Leonel",
						"Distribucion Golosa - Programacion 3", JOptionPane.INFORMATION_MESSAGE);

			}
		});
		btn_info.setBounds(109, 11, 89, 23);
		add(btn_info);

		JButton btnGenerar = new JButton("Optimizar");
		btnGenerar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnGenerar.setBackground(new Color(0, 191, 255));
		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String resp = "";
				int cantCentros = -1;
				int centrosExistentes = panel.getCantidadCentros();
				if (panel.verificarTablasVacias()) {					// NUEVO					
					return;
				}
				while (cantCentros == -1) {
					resp = JOptionPane.showInputDialog(null, "Introduzca la cantidad de centros maximos",
							"Atencion", JOptionPane.INFORMATION_MESSAGE);

					if (resp == null) { //se presiona el boton cancelar
						cantCentros = -1;
						break;
					}
					
					cantCentros = stringToInt(resp);
					
					if (!(cantCentros > 0 && cantCentros <= centrosExistentes)) {
						JOptionPane.showMessageDialog(null,
								"Por favor ingrese un numero entre 1 y " + centrosExistentes);
						cantCentros = -1;
					}

				}
				panel.optimizar(cantCentros);
			}
		});
		btnGenerar.setBounds(300, 557, 89, 40);
		add(btnGenerar);

		JButton btnExportarCentros = new JButton("Exportar Centros");
		btnExportarCentros.setIcon(new ImageIcon(Botonera.class.getResource("/icons/export.png")));
		btnExportarCentros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int resultado = JOptionPane.showConfirmDialog(null,
						"Esta seguro que desea exportar? Podria estar sobreescribiendo un archivo", "Exportar datos",
						dialogButton);
				if (resultado == 0) { // opcion SI
					panel.exportarCentros();
				}
			}
		});
		btnExportarCentros.setBounds(533, 561, 150, 32);
		add(btnExportarCentros);

		JButton btnExportarClientes = new JButton("Exportar Clientes");
		btnExportarClientes.setIcon(new ImageIcon(Botonera.class.getResource("/icons/export.png")));
		btnExportarClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int resultado = JOptionPane.showConfirmDialog(null,
						"Esta seguro que desea exportar? Podria estar sobreescribiendo un archivo", "Exportar datos",
						dialogButton);
				if (resultado == 0) { // opcion SI
					panel.exportarClientes();
				}
			}
		});
		btnExportarClientes.setBounds(149, 561, 150, 32);
		add(btnExportarClientes);

		JButton btnImportarClientes = new JButton("Importar Clientes");
		btnImportarClientes.setIcon(new ImageIcon(Botonera.class.getResource("/icons/import.png")));
		btnImportarClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// preguntar si desea sobreescribir los datos
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int resultado = JOptionPane.showConfirmDialog(null,
						"Esta seguro que desea importar? Sus datos actuales se perderan", "Importar datos",
						dialogButton);
				if (resultado == 0) { // opcion SI
					panel.importarClientes();
				}

			}
		});
		btnImportarClientes.setBounds(5, 561, 150, 32);
		add(btnImportarClientes);

		JButton btnImportarCentros = new JButton("Importar Centros");
		btnImportarCentros.setIcon(new ImageIcon(Botonera.class.getResource("/icons/import.png")));
		btnImportarCentros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// preguntar si desea sobreescribir los datos
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int resultado = JOptionPane.showConfirmDialog(null,
						"Esta seguro que desea importar? Sus datos actuales se perderan", "Importar datos",
						dialogButton);
				if (resultado == 0) { // opcion SI
					panel.importarCentros();
				}

			}
		});
		btnImportarCentros.setBounds(390, 561, 150, 32);
		add(btnImportarCentros);
	}

	private int stringToInt(String string) {

		try {
			if (string.isEmpty())
				return -1;
			return Integer.parseInt(string);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese un numero");
			return -1;
		}
	}
}
